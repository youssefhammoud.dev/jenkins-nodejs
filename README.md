# Implementation JenkinsFile for NodeJs application

Clone the Projet "Jenkins-NodeJs"
``` bash
cd $HOME
git clone https://gitlab.com/youssefhammoud/jenkins-nodejs.git
```


## Dockerization for the nodejs app using Dockerfile 

### Caracteristic for the image

- Image de base : `Node:14`
  
File [Dockerfile](Dockerfile).

### Build the image "jenkins-nodejs"

``` bash
docker build  --no-cache  -t jenkins-nodejs  .
```

### Verification

``` bash
docker images
---
REPOSITORY   TAG    IMAGE ID    CREATED        SIZE
jenkins-nodejs latest 9641655df45 37 seconds ago 971MB
```

##  Start Instance container

From the image created above: 

```
docker run -it  --rm \
    -p 3000:3000 \
    jenkins-nodejs \
    /bin/bash

npm start => to start the application
npm run test => to test all the unit test
npm run coverage:dev => to display the coverage code on each file   
```

Now the application is working successfully on your environment,
you should see something like that:

![datei](/Docs/local-environement.PNG)

# Continuous Integration using jenkins

## Prepare your environement
For the continious Integration we decided to use the Jenkins tool, and for the quality management we decided to use the SonarQube.
The both are open source.

Installing Jenkins directly in your OS can be tricky and expensive in terms of time and resources. You need to have Java installed in your local machine and at least 10 GB of drive space. On the other hand, using docker compose is really straightforward and offers a lot of advantages.

SonarQube is an open source quality management platform, dedicated to continuously analyze and measure technical quality, from project portfolio to method.

Instead of installing the CLI tools in the host machine, we can use a Docker container. The container here will start the code analysis using the CLI tools already installed inside. We just need to start the container, tell it the path to the source code and the url of sonarqube.The project for this container is open source and available here: https://github.com/newtmitch/docker-sonar-scanner. This is useful in CI pipeline because we won’t need to install additional dependencies into the build agent.

SonarQube need Database to store the data, But you can use any one of the supported databases: Oracle, Postgres, SQL Server, and of course MySQL.
For this demo we use Postgres.

### As result, we defined all the services in a docker-compose file
File [docker-compose.yaml](docker-compose.yaml)
```
docker-compose up
```


### Our application is ready for continious Integration
you can check the reusable Jenkinsfile [Jenkinsfile](Jenkinsfile )

```groovy
node {
  //get the commit-Id to use it after to tag the generated image
   def commit_id
   
   stage('Preparation') {
     //get the source code of the application
     git credentialsId: 'gitlab-repo', url: 'https://gitlab.com/youssefhammoud/jenkins-nodejs.git'
     sh "git rev-parse --short HEAD > .git/commit-id"
     commit_id = readFile('.git/commit-id').trim()
     echo "${commit_id}"
   }
   
   stage('test') {
    //use the agent nodejs as container to run and test the application
     def myTestContainer = docker.image('node:14')
     myTestContainer.pull()
     myTestContainer.inside {
          //sh 'make all'
          sh 'pwd'
          sh 'make all'
     }
   }
   
   stage('code coverage') {
       sh 'pwd'
     // generate code coverage link 
      publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'coverage', reportFiles: 'index.html', reportName: 'Code Coverage link', reportTitles: ''])
    }
    
   stage('sonarqube') {
     //configure the sonarqubeplugin to check the quality management of the application
      def sonarqubeScannerHome = tool name: 'sonar', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
      withCredentials([string(credentialsId: 'sonar', variable: 'sonarLogin')]) {
        sh "${sonarqubeScannerHome}/bin/sonar-scanner -e -Dsonar.host.url=http://sonarqube:9000  -Dsonar.login=${sonarLogin} -Dsonar.projectName=parts-store-cli -Dsonar.projectVersion=${env.BUILD_NUMBER} -Dsonar.projectKey=parts-store-cli  -Dsonar.sources=.  -Dsonar.css.node=. -Dsonar.exclusions=src/**/*.spec.js -Dsonar.test=src/**/*.spec.js -Dsonar.exclusions=**/javascripts/dist/*.js,**/javascripts/vendor/*.js,**/stylesheets/*.css  -Dsonar.language=node:14 "
      }
 }

   stage('docker build/push') {  
     //build and push the application  as docker container      
     docker.withRegistry('https://index.docker.io/v1/', 'dockerhub') {
      def app = docker.build("youssefhammoud/docker-nodejs-demo:${commit_id}", '.').push()
     }                                     
  }             
}
```
### The output of the application
is a docker image,
you can check it by typing :

``` bash
docker image ls
---
REPOSITORY                          TAG               IMAGE ID       CREATED          SIZE
youssefhammoud/docker-nodejs-demo   bff9698           b6492ab74d70   4 minutes ago    986MB
```

### For the quality management of the created image 
you can access the SonarQube url: http://localhost:9000/dashboard?id=parts-store-cli

![datei](/Docs/SonarqubeOutput.PNG)

### For the code coverage :
The publishHTML plugin's generate a link like http://localhost:8080/job/my-pip/Code_20Coverage_20link/ 
The output should be like this:

![datei](/Docs/code-coverage.PNG)

### Justification of our Dockerfile(s) and analyse the result of our Pipeline
You can access the file below:
File [/Docs/Integration_NodeJs__with_Jenkins.pdf](/Docs/Integration_NodeJs__with_Jenkins.pdf)
